package hk.quantr.qemuimporter;

import java.io.File;

/**
 *
 * @author peter
 */
public class Data {

    public long address;
    public int lineNo;
    public File file;
    public String code;

    public Data(long address, int lineNo, File file, String code) {
	   this.address = address;
	   this.lineNo = lineNo;
	   this.file = file;
	   this.code = code;
    }
 
}
