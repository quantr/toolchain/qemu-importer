
import hk.quantr.dwarf.dwarf.CompileUnit;
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.dwarf.DwarfDebugLineHeader;
import hk.quantr.dwarf.dwarf.DwarfLib;
import hk.quantr.dwarf.dwarf.DwarfLine;
import hk.quantr.javalib.CommonLib;
import hk.quantr.qemuimporter.Data;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ImportPeterLinux {

	@Test
	public void test() throws SQLException, FileNotFoundException, IOException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		File file = new File("~/workspace/xv6-riscv/cpu.log".replaceFirst("^~", System.getProperty("user.home")));
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line;

		LinkedHashMap<String, String> values;
		long sequence = 0;
//		HashMap<Long, Data> allData = new HashMap<>();
//		initKernelCodeCache(allData);

		Connection conn = DriverManager.getConnection("jdbc:h2:tcp://192.168.0.176//root/h2_qemu_recorder/qemuRecord", "sa", "fuck1234shit");
		boolean createEverything = false;
		boolean createColumn = false;
		Statement stmt = conn.createStatement();
		ArrayList<String> registers = new ArrayList<>();
		String guid = "admin - linux";//UUID.randomUUID().toString();

		try {
			outer:
			while (true) {
				values = new LinkedHashMap<>();
				long pc = -1;
				String code = null;
				String mem = null;
				do {
					line = reader.readLine();
					if (line == null) {
						break outer;
					}
					if (line.startsWith("IN") || line.startsWith("Priv") || line.equals("") || line.contains("----")) {

					} else if (line.startsWith("0x")) {
						code = line;
					} else if (line.startsWith("riscv_cpu_tlb_fill")) {
						if (line.startsWith("riscv_cpu_tlb_fill ad") && (line.contains("rw 0") || line.contains("rw 1"))) {
							mem = line.replaceFirst("riscv_cpu_tlb_fill ad", "");
							mem = mem.replaceAll("mmu_idx.*", "");
							mem = mem.trim();
						}
					} else {
//						System.out.println(line);
						String words[] = line.trim().split(" +");
						for (int x = 0; x < words.length; x += 2) {
							words[x] = words[x].replaceAll("/", "_");
							if (createColumn) {
								registers.add(words[x]);
							}
							String value = words[x + 1].startsWith("0x") ? words[x + 1] : "0x" + words[x + 1];
							values.put(words[x], value);
							if (words[x].trim().equals("pc")) {
								pc = CommonLib.string2long(value);
							}
						}
					}
				} while (!line.startsWith(" x28"));

				if (createEverything) {
					// create column
					if (createColumn) {
						createColumn = false;
						System.out.println("drop table if exists `qemuRecorder_admin`;");
						conn.createStatement().execute("drop table if exists `qemuRecorder_admin`;");
						conn.createStatement().execute("create table `qemuRecorder_admin`(id bigint auto_increment, sequence bigint, guid varchar(50), date datetime, computer varchar(50));");
						for (String register : registers) {
							String sql = "ALTER TABLE `qemuRecorder_admin` ADD `" + register + "` varchar(20) NOT NULL;";
							System.out.println(sql);
							conn.createStatement().execute(sql);
						}

//						conn.createStatement().execute("ALTER TABLE `qemuRecorder_admin` ADD `lineNo` int;");
//						conn.createStatement().execute("ALTER TABLE `qemuRecorder_admin` ADD `filename` varchar(100);");
//						conn.createStatement().execute("ALTER TABLE `qemuRecorder_admin` ADD `path` varchar(1000);");
						conn.createStatement().execute("ALTER TABLE `qemuRecorder_admin` ADD `code` varchar(100);");
						conn.createStatement().execute("ALTER TABLE `qemuRecorder_admin` ADD `mem` varchar(100);");
//						conn.createStatement().execute("ALTER TABLE `qemuRecorder_admin` ADD `mem0` varchar(100);");
//						conn.createStatement().execute("ALTER TABLE `qemuRecorder_admin` ADD `mem1` varchar(100);");
//						conn.createStatement().execute("ALTER TABLE `qemuRecorder_admin` ADD `mem2` varchar(100);");
//						conn.createStatement().execute("ALTER TABLE `qemuRecorder_admin` ADD `mem3` varchar(100);");
						conn.createStatement().execute("drop INDEX if exists sequence_admin on qemuRecorder_admin;");
						conn.createStatement().execute("create INDEX sequence_admin on qemuRecorder_admin (sequence);");
					}
					//end create column
				}

//				Data data = allData.get(pc);
				String sql = "INSERT INTO `qemuRecorder_admin` VALUES (0, " + sequence + ",'" + guid + "',current_date(), 'quantr-ubuntu'";
				for (Map.Entry me : values.entrySet()) {
					sql += ",'" + me.getValue() + "'";
				}
//				if (data == null) {
//					sql += ", null, null, null, null";
//				} else {
				sql += ", '" + code + "'";
				sql += ", '" + mem + "'";
//				}
//				for (String m : mem) {
//					sql += ",'" + m + "'";
//				}
				sql += ")";
				System.out.println(sql);
				stmt.addBatch(sql);

				sequence++;
				if (sequence % 1000 == 0) {
					System.out.printf("%s : %,d\n", sdf.format(new Date()), sequence);
					int[] count = stmt.executeBatch();
					conn.commit();
				}
			}

			int[] count = stmt.executeBatch();
			conn.commit();
		} catch (BatchUpdateException ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
		}
		conn.close();
	}

	private static void initKernelCodeCache(HashMap<Long, Data> allData) {
		File file = new File("~/workspace/xv6-riscv/kernel/kernel".replaceFirst("^~", System.getProperty("user.home")));
		final ArrayList<Dwarf> dwarfVector = DwarfLib.init(file, 0, false);
		for (Dwarf dwarf : dwarfVector) {
			System.out.println(dwarf);
			for (CompileUnit cu : dwarf.compileUnits) {
				System.out.println(cu);
				DwarfDebugLineHeader header = cu.dwarfDebugLineHeader;
				for (DwarfLine line : header.lines) {
					try {
						File f = header.filenames.get((int) line.file_num).file;
						String filename = file.getName();
						String path = f.getAbsolutePath();
						String code = Files.readAllLines(Paths.get(path)).get((int) line.line_num - 1);
						System.out.printf("%d, 0x%08x, %s, %s\n", line.line_num, line.address, path, code);

						allData.put(line.address.longValue(), new Data(line.address.longValue(), line.line_num, f, code));
					} catch (Exception ex) {
						System.out.println(line.line_num);
					}
				}
			}
		}
	}
}
