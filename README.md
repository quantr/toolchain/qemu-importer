# How to generate the qemu log file

QEMUOPTS += -singlestep -d cpu,nochain,in_asm -D peter.log

# Tutorial

https://www.quantr.foundation/2022/08/how-to-record-qemu-into-our-simulator-workbench/
