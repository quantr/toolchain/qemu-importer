
import hk.quantr.dwarf.dwarf.CompileUnit;
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.dwarf.DwarfDebugLineHeader;
import hk.quantr.dwarf.dwarf.DwarfLib;
import hk.quantr.dwarf.dwarf.DwarfLine;
import hk.quantr.peterswing.CommonLib;
import hk.quantr.qemuimporter.Data;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Vector;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ImportKernelToH2 {

    @Test
    public void test() throws Exception {
	   HashMap<Long, Data> allData = new HashMap<>();

	   File file = new File(ImportKernelToMySQL.class.getResource("/xv6/kernel").getPath());
	   final ArrayList<Dwarf> dwarfVector = DwarfLib.init(file, 0, false);
	   for (Dwarf dwarf : dwarfVector) {
		  System.out.println(dwarf);
		  for (CompileUnit cu : dwarf.compileUnits) {
			 System.out.println(cu);
			 DwarfDebugLineHeader header = cu.dwarfDebugLineHeader;
			 for (DwarfLine line : header.lines) {
				try {
				    File f = header.filenames.get((int) line.file_num).file;
				    String filename = file.getName();
				    String path = f.getAbsolutePath();
				    String code = Files.readAllLines(Paths.get(path)).get((int) line.line_num - 1);
				    System.out.printf("%d, 0x%08x, %s, %s\n", line.line_num, line.address, path, code);

				    allData.put(line.address.longValue(), new Data(line.address.longValue(), line.line_num, f, code));
				} catch (Exception ex) {
				    System.out.println(line.line_num);
				}
			 }
		  }
	   }

	   Connection conn = null;
	   conn = DriverManager.getConnection("jdbc:h2:tcp://192.168.0.207/e://qemuRecord", "sa", "");
	   Statement stmt = conn.createStatement();
	   ResultSet rs = stmt.executeQuery("select * from qemuRecorder order by sequence");
	   while (rs.next()) {
		  Long pc = CommonLib.string2long(rs.getString("pc"));
		  Data data = allData.get(pc);
//		  System.out.println(data);
		  if (data != null) {
			 System.out.println(data.lineNo + " - " + String.format("0x%016x", data.address));
			 PreparedStatement ps = conn.prepareStatement("update qemuRecorder set lineNo=?, filename=?, path=?, code=? where pc=?");
			 ps.setInt(1, data.lineNo);
			 ps.setString(2, data.file.getName());
			 ps.setString(3, data.file.getAbsolutePath());
			 ps.setString(4, data.code);
			 ps.setString(5, String.format("0x%016x", data.address));
			 ps.executeUpdate();
		  }
//		  System.out.println(rs.getInt("sequence"));
	   }
	   conn.close();
    }
}
