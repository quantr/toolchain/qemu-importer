
import hk.quantr.dwarf.dwarf.CompileUnit;
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.dwarf.DwarfDebugLineHeader;
import hk.quantr.dwarf.dwarf.DwarfLib;
import hk.quantr.dwarf.dwarf.DwarfLine;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Vector;
import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ImportKernelToMySQL {

    static boolean runInPeterMac = false;
    static String mysql_username;
    static String mysql_password;
    static String mysql_host;
    static String mysql_database;
    static BasicDataSource ds = new BasicDataSource();
    static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    static {
	   if (ImportKernelToMySQL.runInPeterMac) {
		  mysql_username = "assembler";
		  mysql_password = "assembler";
		  mysql_host = "localhost";
		  mysql_database = "assembler";
	   } else {
		  mysql_username = "assembler";
		  mysql_password = "assembler";
		  mysql_host = "192.168.0.174";
		  mysql_database = "assembler";
	   }

	   ImportKernelToMySQL.ds.setUrl("jdbc:mysql://" + ImportKernelToMySQL.mysql_host + "/" + ImportKernelToMySQL.mysql_database + "?rewriteBatchedStatements=true");
	   ImportKernelToMySQL.ds.setUsername(ImportKernelToMySQL.mysql_username);
	   ImportKernelToMySQL.ds.setPassword(ImportKernelToMySQL.mysql_password);
	   ImportKernelToMySQL.ds.setMinIdle(5);
	   ImportKernelToMySQL.ds.setMaxIdle(10);
	   ImportKernelToMySQL.ds.setMaxOpenPreparedStatements(100000);
    }

    @Test
    public void test() throws Exception {
	   Connection conn = null;
	   Class.forName("com.mysql.cj.jdbc.Driver");
	   conn = ImportKernelToMySQL.ds.getConnection();
	   conn.setAutoCommit(true);

	   String sql = "INSERT INTO `kernelCode` VALUES (0, ?, ?, ?, ?, ?)";
	   System.out.println(ImportKernelToMySQL.class.getResource("/xv6/kernel").getPath());
	   File file = new File(ImportKernelToMySQL.class.getResource("/xv6/kernel").getPath());
	   final ArrayList<Dwarf> dwarfVector = DwarfLib.init(file, 0, false);
	   for (Dwarf dwarf : dwarfVector) {
		  System.out.println(dwarf);
		  for (CompileUnit cu : dwarf.compileUnits) {
			 System.out.println(cu);
			 DwarfDebugLineHeader header = cu.dwarfDebugLineHeader;
			 for (DwarfLine line : header.lines) {
				try {
				    String filename = header.filenames.get((int) line.file_num).file.getName();
				    String path = header.filenames.get((int) line.file_num).file.getAbsolutePath();
				    String code = Files.readAllLines(Paths.get(path)).get((int) line.line_num - 1);
				    System.out.printf("%d, 0x%08x, %s, %s\n", line.line_num, line.address, path, code);

				    PreparedStatement ps = conn.prepareStatement(sql);
				    ps.setInt(1, line.line_num);
				    ps.setString(2, filename);
				    ps.setString(3, path);
				    ps.setString(4, code);
				    ps.setLong(5, line.address.longValue());
				    System.out.println(ps.execute());
				} catch (Exception ex) {
				    System.out.println(line.line_num);
				}
			 }
		  }
	   }

	   conn.close();
    }
}
