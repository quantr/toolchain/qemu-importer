
import hk.quantr.qemuimporter.QemuImporter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import org.apache.commons.cli.ParseException;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Test1 {

    @Test
    public void test1() throws ParseException, IOException, FileNotFoundException, ClassNotFoundException, ClassNotFoundException, SQLException {
	   System.out.println(Test1.class.getResource("100000.log").getPath());
//	   QemuImporter.main(("-i " + Test1.class.getResource("100000.log").getPath()).split(" "));
	   QemuImporter.main(("-i /home/peter/workspace/xv6-riscv/cpu.log").split(" "));
    }
}
